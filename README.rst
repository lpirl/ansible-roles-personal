.. image:: https://gitlab.com/lpirl/ansible-roles-personal/badges/master/pipeline.svg
  :target: https://gitlab.com/lpirl/ansible-roles-personal/pipelines
  :align: right

Ansible roles for configuring hosts to my personal preferences.

Feel free to re-use, ask questions, etc.

At some points, the roles in this repository silently assume that at
least the role "core" from `gitlab.com/lpirl/ansible-roles-generic
<https://gitlab.com/lpirl/ansible-roles-generic>`__ has been applied.
However, it is difficult to define dependencies between roles
of different repositories reliably, so no role in this repository
defines a dependency an a role in another repository.


static files
------------

For reference (and easier browsing), all static files of all roles in
this repository (``*/files/*``) can be found `on the pages of this
project <https://lpirl.gitlab.io/ansible-roles-personal/>`__.


conventions
-----------

The `notes on conventions
<https://gitlab.com/lpirl/ansible-roles-generic/-/blob/master/conventions.rst>`__
from `gitlab.com/lpirl/ansible-roles-generic
<https://gitlab.com/lpirl/ansible-roles-generic>`__ also apply here and
might be an interesting read if you consider trying the roles in this
repository.


----

History:

The roles in this repository are a rewrite of
`github.com/lpirl/ansible-roles
<https://github.com/lpirl/ansible-roles>`__
and also some scripts and configuration files previously kept in
`github.com/lpirl/admintools <https://github.com/lpirl/admintools/>`__
are now incorporated here.
