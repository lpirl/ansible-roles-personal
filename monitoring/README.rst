Sets up and configures
`meerkatmon <https://github.com/lpirl/meerkatmon>`__ and
`linkchecker <https://github.com/linkchecker/linkchecker>`__,
as well as cron jobs for regular checks

Hosts need to configured to be able to send emails (e.g., via the role
`lpirl/ansible-roles-generic/mail-transfer-agent
<https://gitlab.com/lpirl/ansible-roles-generic/-/tree/master/mail-transfer-agent>`__).
Specifically the current "implementation" (lightly) depends on Postfix
being installed (to set ``postaliases``).
