This roles installs some basic tools I use regularly.

If a system is not installed with a minimal set of pre-installed
packages, most of the packages installed by this role are present
anyway.
