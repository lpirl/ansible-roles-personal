- name: install required packages for zsh configuration
  package:
    name: "{{ item }}"
  with_items:
    - git
    - zsh


- name: clone code to style the prompt
  git:
    repo: 'https://github.com/agkozak/agkozak-zsh-prompt.git'
    dest: '/opt/agkozak-zsh-prompt'
    version: master
    umask: '022'
    depth: 3
    force: yes


- name: prevent Debian keyboard changes
  blockinfile:
    path: "/etc/zsh/zshenv"
    create: yes
    mode: a+r
    marker: "# {mark} ANSIBLE MANAGED BLOCK (prevent keyboard changes)"
    block: |
      DEBIAN_PREVENT_KEYBOARD_CHANGES=yes

# remove duplicate block above, which used default marker before
- name: clean up legacy (Jul 22)
  blockinfile:
    path: "/etc/zsh/zshenv"
    state: absent



- name: write zsh configuration
  blockinfile:
    path: "/{{ item }}"
    block: "{{ lookup('file', item) }}"
    mode: a+r
    create: yes
  with_items:
    - etc/zsh/zshrc



- name: make zsh the default shell for new users (adduser)
  lineinfile:
    dest: /etc/adduser.conf
    regexp: '^\s*DSHELL\s*=\s*'
    line: 'DSHELL=/usr/bin/zsh'

- name: make zsh the default shell for new users (useradd)
  lineinfile:
    dest: /etc/default/useradd
    regexp: '^\s*SHELL\s*=\s*'
    line: 'SHELL=/usr/bin/zsh'

- name: set zsh as shell for current non-root user
  user:
    name: '{{ ansible_env.SUDO_USER | default(ansible_user_id) }}'
    shell: /usr/bin/zsh
  when: ansible_env.SUDO_USER | default(ansible_user_id) != "root"

- name: set zsh as shell for root
  user:
    name: root
    shell: /usr/bin/zsh



- name: create /opt/bin, as directory for non-package binaries
  file:
    dest: /opt/bin
    state: directory
    mode: u=rwx,go=rx

- name: add /opt/bin to PATH
  blockinfile:
    path: "{{ item.key }}"
    create: yes
    mode: a+r
    marker: "# {mark} ANSIBLE MANAGED BLOCK (add /opt/bin to PATH)"
    block: "{{ item.value }}"
  with_dict:
    /etc/profile.d/opt-bin.sh: |
      test "${PATH#*/opt/bin}" != "$PATH" \
      || export PATH=/opt/bin:$PATH
    /etc/zsh/zshenv: |
      path=('/opt/bin' $path)
      export PATH
  loop_control:
    label: "{{ item.key }}"
